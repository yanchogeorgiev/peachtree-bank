import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {

  constructor(private routes: Router) { }

  canActivate(): boolean {
    if (localStorage.getItem('token')) {
      return true;
    } else {
      this.routes.navigate(['/login']);
      return false;
    }
  }
}
