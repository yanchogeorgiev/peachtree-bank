export interface AuthDto {
    token: string;
    expires_in: number;
}
