import { Injectable } from '@angular/core';
import { tap, shareReplay } from 'rxjs/operators';
import { AuthService } from '../core/auth.service';
import { of, throwError, Observable } from 'rxjs';

interface AuthResponse {
  data: {
    token: string;
    expires_in: number;
  };
}

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private authService: AuthService
  ) {
  }

  login(email: string, password: string) {
    return new Observable<AuthResponse>(subscriber => {
      if (email !== 'demo' || password !== 'demo') {
        subscriber.error();
      } else {
        subscriber.next({
          data: {
            token: 'asdf',
            expires_in: 3600
          }
        });
        subscriber.complete();
      }
    }).pipe(
      tap(res => this.authService.authenticate(res.data)),
      shareReplay()
    );
  }
}
