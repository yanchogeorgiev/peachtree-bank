import { LoginService } from './login.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  msg = '';

  constructor(
    private routes: Router,
    private loginService: LoginService
    ) { }

  login(email: string, password: string) {
    this.loginService.login(email, password).subscribe(
      () => this.routes.navigate(['/transfers']),
      () => this.msg = 'Invalid Username or Password'
    );
  }
}
