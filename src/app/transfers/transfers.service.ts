import { Transfer, PaymentType, PaymentStatus } from './transfers.dto';
import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class TransfersService {

  transfers$ = new BehaviorSubject<Transfer[]>([{
    date: new Date(),
    type: PaymentType.card,
    title: 'Transaction 1',
    amount: 100,
    to: 'Company 1',
    status: PaymentStatus.sent
  }, {
    date: new Date(),
    type: PaymentType.cash,
    title: 'Transaction 2',
    amount: 100,
    to: 'Company 2',
    status: PaymentStatus.received
  }, {
    date: new Date(),
    type: PaymentType.cash,
    title: 'Transaction 3',
    amount: 100,
    to: 'Company 3',
    status: PaymentStatus.delivered
  }, {
    date: new Date(),
    type: PaymentType.card,
    title: 'Transaction 4',
    amount: 100,
    to: 'Company 4',
    status: PaymentStatus.sent
  }]);

  constructor() { }
}
