import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransfersRoutingModule } from './transfers-routing.module';
import { TransferCreateComponent } from './transfer-create/transfer-create.component';
import { TransferViewComponent } from './transfer-view/transfer-view.component';
import { TransfersComponent } from './transfers.component';
import { TransfersListComponent } from './transfers-list/transfers-list.component';


@NgModule({
  declarations: [
    TransferCreateComponent,
    TransferViewComponent,
    TransfersComponent,
    TransfersListComponent
  ],
  imports: [
    CommonModule,
    TransfersRoutingModule,
    MatButtonModule,
    MatCardModule,
    MatTableModule,
  ]
})
export class TransfersModule { }
