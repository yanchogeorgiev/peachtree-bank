export interface Transfer {
    date: Date;
    type: PaymentType;
    title: string;
    amount: number;
    to: string;
    status: PaymentStatus;
}

export enum PaymentType {
    card = 'card',
    cash = 'cash'
    // ...
}

export enum PaymentStatus {
    sent = 'sent',
    received = 'received',
    delivered = 'delivered'
    // ...
}
