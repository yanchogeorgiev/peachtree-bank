import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TransfersComponent } from './transfers.component';
import { TransfersListComponent } from './transfers-list/transfers-list.component';
import { TransferViewComponent } from './transfer-view/transfer-view.component';


const routes: Routes = [{
  path: '',
  component: TransfersComponent,
  children: [
    {
      path: '',
      component: TransfersListComponent,
    },
    {
      path: 'view',
      component: TransferViewComponent,
    },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransfersRoutingModule { }
