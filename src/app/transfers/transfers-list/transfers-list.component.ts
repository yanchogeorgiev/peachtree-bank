import { PaymentType, PaymentStatus } from './../transfers.dto';
import { TransfersService } from './../transfers.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-transfers-list',
  templateUrl: './transfers-list.component.html',
  styleUrls: ['./transfers-list.component.scss']
})
export class TransfersListComponent implements OnInit {
  displayedColumns: string[] = ['status', 'date', 'name', 'amount'];
  dataSource = this.transfersService.transfers$;
  types = {
    [PaymentType.card]: 'Card Payment',
    [PaymentType.cash]: 'Cash Payment',
  };
  statuses = {
    [PaymentStatus.sent]: 'red',
    [PaymentStatus.received]: 'orange',
    [PaymentStatus.delivered]: 'green',
  };

  constructor(
    private transfersService: TransfersService,
  ) { }

  ngOnInit(): void {
  }

}
